package com.abc.cardservice.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CardProduct implements Serializable {

    private Integer productId;
    private String productCode;
    private String productName;
    private String productType;
    private String createdBy;
    private Date createdTime;
    private String updatedBy;
    private Date updatedTime;

}
