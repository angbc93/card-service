package com.abc.cardservice.controller;


import com.abc.cardservice.model.CardResponse;
import com.abc.cardservice.service.CardService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CardController {

    private CardService cardService;
    private HttpHeaders httpHeaders;

    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    @GetMapping(path = "/card-service")
    public ResponseEntity cardInquiry() {

        try {
            CardResponse cardResponse = cardService.getAllAccount();
            return new ResponseEntity<>(cardResponse, httpHeaders, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(e, httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }


}


