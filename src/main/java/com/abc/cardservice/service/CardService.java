package com.abc.cardservice.service;


import com.abc.cardservice.model.Card;
import com.abc.cardservice.model.CardProduct;
import com.abc.cardservice.model.CardResponse;
import com.abc.cardservice.repository.CardDAO;
import com.abc.cardservice.repository.CardProductDAO;
import com.abc.cardservice.repository.CardProductRepo;
import com.abc.cardservice.repository.CardRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CardService {
    private CardRepo cardRepo;
    private CardProductRepo cardProductRepo;

    public CardService(CardRepo cardRepo, CardProductRepo cardProductRepo) {
        this.cardRepo = cardRepo;
        this.cardProductRepo = cardProductRepo;
    }

    public CardResponse getAllAccount() {
        CardResponse cardResponse = new CardResponse();
        cardResponse.setCards(new ArrayList<>());

        List<CardDAO> cardDAOList = cardRepo.findAll();
        cardDAOList.forEach(cardDAO -> {
            Card card = new Card();
            CardProduct cardProduct = new CardProduct();
            card.setCardProduct(cardProduct);


            CardProductDAO cardProductDAO = cardProductRepo.findProductById(cardDAO.getProductId());
            cardResponse.getCards().add(accountMapper(card, cardDAO, cardProductDAO));

        });

        return cardResponse;

    }


    private Card accountMapper(Card card, CardDAO cardDAO, CardProductDAO cardProductDAO) {
        card.setCardNo(cardDAO.getCardNo());
        card.setIsHidden(cardDAO.getIsHidden());
        card.setCreatedBy(cardDAO.getCreatedBy());
        card.setCreatedTime(cardDAO.getCreatedTime());
        card.setUpdatedBy(cardDAO.getUpdatedBy());
        card.setUpdatedTime(cardDAO.getUpdatedTime());

        card.getCardProduct().setProductId(cardProductDAO.getProductId());
        card.getCardProduct().setProductCode(cardProductDAO.getProductCode());
        card.getCardProduct().setProductName(cardProductDAO.getProductName());
        card.getCardProduct().setProductType(cardProductDAO.getProductType());
        card.getCardProduct().setCreatedBy(cardProductDAO.getCreatedBy());
        card.getCardProduct().setCreatedTime(cardProductDAO.getCreatedTime());
        card.getCardProduct().setUpdatedBy(cardProductDAO.getUpdatedBy());
        card.getCardProduct().setUpdatedTime(cardProductDAO.getUpdatedTime());

        return card;
    }


}
