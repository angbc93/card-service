package com.abc.cardservice.controller;

import com.abc.cardservice.model.CardResponse;
import com.abc.cardservice.service.CardService;
import com.abc.cardservice.service.CardServiceTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CardControllerTest {

    @InjectMocks
    private CardController cardController;
    @Mock
    private CardService cardService;
    @Mock
    private HttpHeaders httpHeaders;

    @Test
    public void cardInquiry() {

        CardResponse cardResponse = new CardResponse();

        Mockito.when(cardService.getAllAccount()).thenReturn(cardResponse);

        ResponseEntity actual = cardController.cardInquiry();

        assertEquals(cardResponse,actual.getBody());
        assertEquals(HttpStatus.OK,actual.getStatusCode());

    }

    @Test
    public void cardInquiryFail() {

        Mockito.when(cardService.getAllAccount()).thenThrow(new RuntimeException());

        ResponseEntity actual = cardController.cardInquiry();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,actual.getStatusCode());

    }

}