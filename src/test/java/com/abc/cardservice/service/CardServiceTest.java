package com.abc.cardservice.service;

import com.abc.cardservice.model.Card;
import com.abc.cardservice.model.CardProduct;
import com.abc.cardservice.model.CardResponse;
import com.abc.cardservice.repository.CardDAO;
import com.abc.cardservice.repository.CardProductDAO;
import com.abc.cardservice.repository.CardProductRepo;
import com.abc.cardservice.repository.CardRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CardServiceTest {


    @Mock
    private CardRepo cardRepo;
    @Mock
    private CardProductRepo cardProductRepo;
    @InjectMocks
    private CardService cardService;

    @Test
    public void getAllAccount() {

        CardDAO cardDao = new CardDAO();
        CardProductDAO cardProductDAO = new CardProductDAO();
        List<CardDAO> cardDAOList = new ArrayList<>();

        cardDao.setCardNo("112233");
        cardDao.setIsHidden(Boolean.FALSE);
        cardDao.setProductId(1);
        cardDAOList.add(cardDao);

        cardProductDAO.setProductId(1);
        cardProductDAO.setProductCode("001VISA");
        cardProductDAO.setProductName("Visa Credit Card");
        cardProductDAO.setProductType("Visa");

        //Expected
        CardResponse cardResponse = new CardResponse();
        CardProduct cardProduct = new CardProduct();
        Card card = new Card();

        card.setCardProduct(cardProduct);
        cardResponse.setCards(new ArrayList<>());

        card.setCardNo("112233");
        card.setIsHidden(Boolean.FALSE);
        card.getCardProduct().setProductId(1);
        card.getCardProduct().setProductCode("001VISA");
        card.getCardProduct().setProductName("Visa Credit Card");
        card.getCardProduct().setProductType("Visa");
        cardResponse.getCards().add(card);

        when(cardRepo.findAll()).thenReturn(cardDAOList);
        when(cardProductRepo.findProductById(1)).thenReturn(cardProductDAO);

        CardResponse actual = cardService.getAllAccount();

        assertEquals(cardResponse, actual);

    }


}